package com.example.ayca.githubretrodeneme.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Owner(
    @Expose
    @SerializedName("id")
    val id: Int,
    @Expose
    @SerializedName("login")
    val login: String,
    @Expose
    @SerializedName("avatar_url")
    val avatar_url: String,
    @Expose
    @SerializedName("type")
    val type: String

)