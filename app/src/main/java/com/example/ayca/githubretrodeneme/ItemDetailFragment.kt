package com.example.ayca.githubretrodeneme

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation

class ItemDetailFragment : Fragment() {
    lateinit var avatarImg: ImageView
    lateinit var fullNameTxt: TextView
    lateinit var languageTxt: TextView
    lateinit var idTxt: TextView
    lateinit var wathersTxt: TextView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.item_detail_fragment, container, false)
        initView(view)


        return view
    }

    private fun initView(view: View) {
        avatarImg = view.findViewById(R.id.owner_avatar_img)
        fullNameTxt = view.findViewById(R.id.owner_fullname_txt)
        languageTxt = view.findViewById(R.id.language_txt)
        idTxt = view.findViewById(R.id.owner_id_txt)
        wathersTxt = view.findViewById(R.id.watchers_txt)

        fullNameTxt.text = arguments?.getString("fullName").toString()
        languageTxt.text = arguments?.getString("language").toString()
        idTxt.text = arguments?.getString("id").toString()
        wathersTxt.text = arguments?.getString("watchers").toString()

        val avatarUrl = arguments?.getString("avatarUrl").toString()
        Picasso.get().load(avatarUrl).resize(250, 250).transform(CropCircleTransformation())
            .into(avatarImg)
    }

}