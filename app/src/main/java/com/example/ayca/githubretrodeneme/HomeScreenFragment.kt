package com.example.ayca.githubretrodeneme

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.githubretrodeneme.adapter.DataAdapter
import com.example.ayca.githubretrodeneme.model.Items
import com.example.ayca.githubretrodeneme.model.ResponseModel
import com.example.ayca.githubretrodeneme.retrofit.ApiClient
import retrofit2.Call
import retrofit2.Response
import retrofit2.Callback


class HomeScreenFragment : Fragment() {
    lateinit var recyclerView: RecyclerView
    lateinit var itemList: List<Items>
    lateinit var adapter: DataAdapter
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.home_screen_fragment, container, false)
        initView(view)
        getData()
        return view
    }

    private fun initView(view: View) {
        recyclerView = view.findViewById(R.id.reycler_view)
        recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

    }

    private fun getData() {
        val call: Call<ResponseModel> = ApiClient.getClient.getJSON()
        call.enqueue(object : Callback<ResponseModel> {
            override fun onFailure(call: Call<ResponseModel>, t: Throwable) {

            }

            override fun onResponse(call: Call<ResponseModel>, response: Response<ResponseModel>) {
                itemList = response.body()!!.items
                if (response.isSuccessful) {
                    adapter = DataAdapter(itemList, context!!)
                    recyclerView.adapter = adapter
                }
            }

        }
        )
    }
}