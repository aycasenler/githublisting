package com.example.ayca.githubretrodeneme.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Items(
    @Expose
    @SerializedName("id")
    val id:Int,
    @Expose
    @SerializedName("watchers")
    val watchers : Int,
    @Expose
    @SerializedName("fork")
    val fork : Boolean,
    @Expose
    @SerializedName("name")
    val name : String,
    @Expose
    @SerializedName("full_name")
    val full_name : String,
    @Expose
    @SerializedName("language")
    val language :String,
    @Expose
    @SerializedName("owner")
    val owner :Owner

)