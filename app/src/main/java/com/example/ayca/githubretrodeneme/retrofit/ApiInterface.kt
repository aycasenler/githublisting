package com.example.ayca.githubretrodeneme.retrofit

import com.example.ayca.githubretrodeneme.model.ResponseModel
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {

@GET("search/repositories?q=tetris+language:assembly&sort=stars&order=desc")
fun getJSON() : Call<ResponseModel>
}