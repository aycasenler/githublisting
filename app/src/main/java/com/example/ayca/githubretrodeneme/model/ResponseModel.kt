package com.example.ayca.githubretrodeneme.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ResponseModel(
@Expose
@SerializedName("total_count")
val total_count :Int,
@Expose
@SerializedName("incomplete_results")
val incomplete_results : Boolean,
@Expose
@SerializedName("items")
val items : List<Items>
)