package com.example.ayca.githubretrodeneme.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.githubretrodeneme.R
import com.example.ayca.githubretrodeneme.model.Items
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation

class DataAdapter(private var itemList: List<Items>, private var context: Context) :
    RecyclerView.Adapter<DataAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_design, parent, false))
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: DataAdapter.ViewHolder, position: Int) {

        val items = itemList.get(position)
        holder.fullnameTxt.text = items.full_name
        holder.languageTxt.text = items.language
        holder.typeTxt.text = items.owner.type
        holder.watchersTxt.text = items.watchers.toString()
        picassoImage(holder.avatarImg, items.owner.avatar_url)

        holder.itemView.setOnClickListener() {
            clickItem(items, it)
        }
    }

    @SuppressLint("ResourceType")
    private fun clickItem(
        item: Items,
        it: View
    ) {
        val position = itemList.indexOf(item)
        val bundle = Bundle()

        bundle.putString("fullName", itemList[position].full_name)
        bundle.putString("language", itemList[position].language)
        bundle.putString("id", itemList[position].owner.id.toString())
        bundle.putString("watchers", itemList[position].watchers.toString())
        bundle.putString("avatarUrl", itemList[position].owner.avatar_url)


        Navigation.findNavController(it).navigate(R.id.action_homeScreenFragment_to_itemDetailFragment, bundle)
    }


    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var fullnameTxt: TextView
        var languageTxt: TextView
        var typeTxt: TextView
        var watchersTxt: TextView
        lateinit var avatarImg: ImageView

        init {
            fullnameTxt = itemLayoutView.findViewById(R.id.fullname_txt)
            languageTxt = itemLayoutView.findViewById(R.id.language_txt)
            typeTxt = itemLayoutView.findViewById(R.id.type_txt)
            watchersTxt = itemLayoutView.findViewById(R.id.watchers_txt)
            avatarImg = itemLayoutView.findViewById(R.id.avatar_img)

        }
    }

    fun picassoImage(listDesing: ImageView, url: String) {
        Picasso.get().load(url).resize(250, 250).transform(CropCircleTransformation())
            .into(listDesing)
    }
}